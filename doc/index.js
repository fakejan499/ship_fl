import Chart from "chart.js/auto";
import {shipPosition, shipRotation} from "../js/fuzzy-controller";

const COLORS = [
  '#4dc9f6',
  '#f67019',
  '#f53794',
  '#537bc4',
  '#acc236',
  '#166a8f',
  '#00a950',
  '#58595b',
  '#8549ba'
];

function chartForFL(key, datasource, namesList, options) {
  const ctx = document.getElementById(`${key}-chart`).getContext('2d');
  const labels = [...datasource[0]];
  const data = {
    labels,
    datasets: [...datasource.slice(1).map((r, i) => ({
      label: namesList[i],
      data: [...r],
      borderColor: COLORS[i],
      backgroundColor: COLORS[i],
      fill: false,
      pointRadius: 0
    }))]
  };
  const config = {
    type: 'line',
    data: data,
    options: {
      ...options,

    }
  };

  const chart = new Chart(
    ctx,
    config
  );
}

// ASSEMBLY LINE
chartForFL('input', shipPosition, ['far left', 'left', 'low left', 'center', 'low right', 'right', 'far right'], {
  scales: {
    x: {
      title: {
        text: 'Ship position',
        align: 'end',
        display: true
      },
      ticks: {
        stepSize: .5,
        autoSkip: true,
        maxTicksLimit: 20,
      }
    },
    y: {
      title: {
        text: 'u(shipPosition)',
        align: 'end',
        display: true
      }
    }
  }
});
chartForFL('output', shipRotation, ['high rotation left', 'medium rotation left', 'low rotation left', 'center', 'low rotation right', 'medium rotation right', 'high rotation right'], {
  scales: {
    x: {
      title: {
        text: 'Ship rotation',
        align: 'end',
        display: true
      },
      ticks: {
        stepSize: 1,
        autoSkip: true,
        maxTicksLimit: 20,
      },
      min: -.3,
      max: .3
    },
    y: {
      title: {
        text: 'u(shipRotation)',
        align: 'end',
        display: true
      },
    }
  }
});


