import * as THREE from 'three';

const trunkHeight = 8;
const trunkGeometry = new THREE.BoxGeometry(1, trunkHeight, 1);
const trunkMaterial = new THREE.MeshPhongMaterial({color: 'rgb(153, 146, 130)'});
const leafGeometry = new THREE.SphereGeometry(1, 3, 12, 0, 0.85, 0, Math.PI * 3/4);
leafGeometry.scale(1, 2.5, 1);
leafGeometry.rotateZ(-Math.PI / 2);
leafGeometry.rotateX(-.85 / 2);
leafGeometry.translate(2, trunkHeight / 3, 0);
const leafMaterial = new THREE.MeshPhongMaterial({color: '#6f9940', side: THREE.DoubleSide});

export const palms = new Set();

export class Palm extends THREE.Object3D {
  constructor() {
    super();

    this.trunk = new THREE.Mesh(trunkGeometry, trunkMaterial);
    this.trunk.rotateX(Math.PI / 2);
    this.trunk.position.z = trunkHeight / 2;

    this.addLeafsToTrunk();

    this.add(this.trunk);

    this.rotateZ(Math.random() * Math.PI);

    this.scale.z = Math.random() * .8 + .8;

    palms.add(this);

    this.addEventListener('removed', () => {
      palms.delete(this);
    })
  }

  addLeafsToTrunk() {
    const step = Math.PI / 8;

    for (let i = -.5; i < 1; i += .5) {
      let angle = 0;

      while (angle < Math.PI * 2) {
        this.addLeafToTrunk(i, angle);
        angle += step;
      }
    }

  }

  addLeafToTrunk(translateY, leafRotation) {
    const leaf = new THREE.Mesh(leafGeometry, leafMaterial);
    leaf.translateY(Math.random() * translateY);
    leaf.rotateY(leafRotation);

    this.trunk.add(leaf);
  }

  clear() {
    palms.delete(this);

    return super.clear();
  }
}
