export const shipPosition = [[], [], [], [], [], []];
export const shipRotation = [[], [], [], [], [], []];
export let shipRotationAppliedRules = [[], [], [], [], [], []];

// przyjete dla testow wartosci
const riverWidth = 10;
const stepsMultiplier = 100;

const generateTriangleShape = function (valuesOX, a, x0, b) {
  return generateTrapezoidShape(valuesOX, a, x0, x0, b);
};

const generateTrapezoidShape = function (valuesOX, a, x1, x2, b) {
  let membershipFunctionValues = [];
  let value;
  valuesOX.forEach((pos, i) => {
    if (pos >= a && pos <= x1 && a !== x1) value = (pos - a) / (x1 - a);
    else if (pos >= x1 && pos <= x2) value = 1;
    else if (pos >= x2 && pos <= b && b !== x2) value = (b - pos) / (b - x2);
    else value = 0;
    membershipFunctionValues[i] = value;
  });
  return membershipFunctionValues;
};

// Init Ship Position OX
shipPosition[0] = [...new Array(riverWidth * stepsMultiplier + 1)].map(
  (_, index) => index / stepsMultiplier
);

shipPosition[1] = generateTriangleShape(shipPosition[0], 0, 0, 3); // Far left
shipPosition[2] = generateTriangleShape(shipPosition[0], 1.25, 3, 4.75); // Left
shipPosition[3] = generateTriangleShape(shipPosition[0], 4.5, 4.75, 5); // Low Left
shipPosition[4] = generateTriangleShape(shipPosition[0], 4.75, 5, 5.25); // Center
shipPosition[5] = generateTriangleShape(shipPosition[0], 5, 5.25, 5.5); // Low Right
shipPosition[6] = generateTriangleShape(shipPosition[0], 5.25, 7, 8.75); // Right
shipPosition[7] = generateTriangleShape(shipPosition[0], 7, 10, 10); // Far right

// Init Ship Rotation OX
// TO.DO DO IT BETTER
let num = -2.5;
for (;;) {
  shipRotation[0].push(num);
  num += 0.01;
  num = +num.toFixed(2);
  if (num === 2.51) break;
}
// shipRotationAppliedRules[0] = shipRotation[0];

shipRotation[1] = generateTrapezoidShape(shipRotation[0], -2.5, -2.5, -0.2, -0.15); // High Rotation Left
shipRotation[2] = generateTrapezoidShape(shipRotation[0], -0.22, -0.15, -0.1, -0.03); // Medium Rotation left
shipRotation[3] = generateTrapezoidShape(shipRotation[0], -0.05, -0.03, -0.01, 0); // Low Rotation left
shipRotation[4] = generateTriangleShape(shipRotation[0], -0.01, 0, 0.01); // Center
shipRotation[5] = generateTrapezoidShape(shipRotation[0], 0, 0.01, 0.03, 0.05); // Low Rotation Right
shipRotation[6] = generateTrapezoidShape(shipRotation[0], 0.03, 0.1, 0.15, 0.22); // Medium Rotation Right
shipRotation[7] = generateTrapezoidShape(shipRotation[0], 0.15, 0.2, 2.5, 2.5); // High Rotation Right

// Init rules
const rules = new Map();
// INPUT --> 0 - FAR LEFT, 1 - LEFT, 2 - CENTER, 3 - RIGHT, 4 - FAR RIGHT
// OUTPUT --> 0 - HIGH ROTATION LEFT, 1 - LOW ROTATION LEFT, 2 - CENTER, 3 - LOW ROTATION RIGHT, 4 - HIGH ROTATION RIGHT
rules.set(0, 6); // IF SHIP POSITION = FAR LEFT -> SHIP ROTATION = HIGH ROTATION RIGTH
rules.set(1, 5); // ...
rules.set(2, 4);
rules.set(3, 3);
rules.set(4, 2);
rules.set(5, 1);
rules.set(6, 0);

// fuzzifier - input set ([0] - OX values, [1..n] - membership function values)
export const fuzzifyInput = function (fuzzifier, input) {
  const inputPosition = fuzzifier[0].findIndex((x) => x === input);
  const fuzzyValue = [];
  for (let i = 1; i < fuzzifier.length; i++) {
    fuzzyValue.push(fuzzifier[i][inputPosition]);
  }

  return fuzzyValue;
};

// output set ([0] - OX values)
// rules - rules map(key - input, value - output)
// DANGER! IMPURE FUNCTION (+ works only for this specific case -- 1 input --> 1 output) -- TO.DO FIX
const makeInference = function (outputSet, fuzzyInput, rules) {
  const fuzzyResult = [];
  const outputSetAppliedRules = new Array(outputSet.length)
    .fill(0)
    .map(() => new Array(outputSet[0].length).fill(0));

  outputSet[0].forEach((val, i) => {
    outputSetAppliedRules[0][i] = val;

    let maxValue = 0;
    for (const [input, output] of rules.entries()) {
      outputSetAppliedRules[output + 1][i] = Math.min(
        outputSet[output + 1][i],
        fuzzyInput[input]
      );

      maxValue =
        outputSetAppliedRules[output + 1][i] > maxValue
          ? outputSetAppliedRules[output + 1][i]
          : maxValue;
    }

    fuzzyResult[i] = maxValue;
  });
  return fuzzyResult;
};

// defuzzifier - output set ([0] - OX values)
const deffuzifyOutput = function (deffuzifier, output) {

  // center of gravity method
  const {numerator, denominator} = output.reduce((sums, val, i) => {
    sums.numerator += deffuzifier[0][i] * val;
    sums.denominator += val;
    return sums;
  }, {numerator: 0, denominator: 0});

  const result = numerator / denominator;
  return result;
};

export const shipPositionToShipRotation = function (input) {
  const fuzzyInput = fuzzifyInput(shipPosition, input);
  const fuzzyResult = makeInference(shipRotation, fuzzyInput, rules);
  const output = deffuzifyOutput(shipRotation, fuzzyResult);
  return output;
};

console.log(shipPosition[0])
