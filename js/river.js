import * as THREE from "three";
import * as camera from "./camera";
import {environmentSpeed} from "./environment";
import {cacti} from "./cactus";
import {palms} from "./palm";

let additionalOffsetBuffer = 0;
let offsetX = 0;
const maxOffsetX = 60;
const minOffsetX = -maxOffsetX;

const riverStartZ = camera.z;
const riverEndZ = camera.far;
const riverLength = Math.abs(riverStartZ) + Math.abs(riverEndZ) + 20;
const riverWidthStep = 2;
const riverMinWidth = 8;
const riverMaxWidth = 100;
let riverWidth = 12;

const depth = environmentSpeed;

const geometry = new THREE.BoxGeometry(1, 1, depth);
const material = new THREE.MeshPhongMaterial({color: 0x4dabf7, side: THREE.DoubleSide});
const riverObjects = generateObjects();
export const river = new THREE.Group();

riverObjects.forEach(o => river.add(o));

function generateObjects() {
  return new Array(Math.ceil(riverLength / depth)).fill(null).map((el, i) => {
    const mesh = new THREE.Mesh(geometry, material);
    mesh.position.z = camera.z - i * depth;
    mesh.scale.x = riverWidth;
    // This solution is dumb as f... but it solves problem with plants on the river in a simplistic way.
    setTimeout(() => removePlantsOnRiver(mesh, riverWidth, depth), 0);
    return mesh;
  })
}

function updateOffset() {
  offsetX += additionalOffsetBuffer;
  offsetX = Math.max(Math.min(offsetX, maxOffsetX), minOffsetX);
  additionalOffsetBuffer = 0;
}

function removeFirstRiverSegment() {
  const objectToClear = riverObjects.shift();
  river.remove(objectToClear);
  objectToClear.clear();
}

function addNewRiverSegment() {
  const newRiverSegment = new THREE.Mesh(geometry, material);
  newRiverSegment.scale.x = riverWidth;
  newRiverSegment.position.z = riverObjects[riverObjects.length - 1].position.z - depth;
  newRiverSegment.position.x = offsetX;
  riverObjects.push(newRiverSegment);
  river.add(newRiverSegment);

  removePlantsOnRiver(newRiverSegment, riverWidth, depth);
}

function removePlantsOnRiver(riverSegment, riverWidth, riverDepth) {
  const {x: riverX, z: riverZ} = riverSegment.position;
  const plantSets = [cacti, palms];
  const minSpaceAroundRiver = 1;

  for (const plantSet of plantSets) {
    for (const plant of plantSet.values()) {
      const {x: plantX, z: plantZ} = plant.localToWorld(new THREE.Vector3());

      if (!(plantX >= riverX + riverWidth / 2 + minSpaceAroundRiver || plantX <= riverX - riverWidth / 2 - minSpaceAroundRiver) &&
        !(plantZ >= riverZ + riverDepth || plantZ <= riverZ - riverDepth)) {
        plant.clear();
      }
    }
  }
}

function moveRiverSegments(speedMul) {
  riverObjects.forEach(o => o.position.z += depth * speedMul);
}

export function moveRiver(speedMul) {
  if (riverObjects[0].position.z > riverStartZ) {
    updateOffset();
    removeFirstRiverSegment();
    addNewRiverSegment();
  }

  moveRiverSegments(speedMul);
}

function increaseRiverWidth() {
  riverWidth += riverWidthStep;
  ensureCorrectWidth();
}

function decreaseRiverWidth() {
  riverWidth -= riverWidthStep;
  ensureCorrectWidth();
}

function ensureCorrectWidth() {
  riverWidth = Math.max(Math.min(riverWidth, riverMaxWidth), riverMinWidth);
}

document.addEventListener("keydown", ({key}) => {
  switch (key) {
    case "ArrowLeft":
      additionalOffsetBuffer = Math.max(additionalOffsetBuffer - depth * 2, -riverWidth / 4);
      break;
    case "ArrowRight":
      additionalOffsetBuffer = Math.min(additionalOffsetBuffer + depth * 2, riverWidth / 4);
      break;
    case "ArrowUp":
      increaseRiverWidth();
      break;
    case "ArrowDown":
      decreaseRiverWidth();
      break;
  }
});

if ('ontouchstart' in window || navigator.maxTouchPoints > 0 || true) {
  let interval;

  document.addEventListener('touchstart', ({touches: [{clientX: x, clientY : y}]}) => {
    const {innerWidth, innerHeight} = window;

    interval = setInterval(() => {
      if (x < innerWidth / 4) additionalOffsetBuffer = Math.max(additionalOffsetBuffer - depth * 2, -riverWidth / 4);
      else if (x > innerWidth / 4 * 3) additionalOffsetBuffer = Math.min(additionalOffsetBuffer + depth * 2, riverWidth / 4);
      else if (y < innerHeight / 3) increaseRiverWidth();
      else if (y > innerHeight / 3 * 2) decreaseRiverWidth();
    }, 20);


  });

  document.addEventListener('touchend', () => {
    clearInterval(interval);
  })
}
