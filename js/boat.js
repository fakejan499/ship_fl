import * as THREE from "three";
import { environmentSpeed } from "./environment";
import {boatCamera, boatSteeringCamera} from "./camera";

export const boat = new THREE.Group();

// SIZES

const boatWidth = 3;
const boatHeight = 2.5;
export const boatLength = 3.5;
const bowLength = 4;
const mastRadius = 0.2;
const mastHeight = 8;
const mastSupportLength = 6;

// GEOMETRY

const bodyGeometry = new THREE.BoxGeometry(boatWidth, boatHeight, boatLength);

const mastGeometry = new THREE.CylinderBufferGeometry(
  mastRadius / 2,
  mastRadius,
  mastHeight,
  20
);

const mastSupportGeometry = new THREE.CylinderBufferGeometry(
  mastRadius / 4,
  mastRadius / 2,
  mastSupportLength,
  20
);

const steeringWheelGeometry = new THREE.TorusGeometry(.3, .03, 16, 16);
const steeringWheelBasisGeometry = new THREE.BoxGeometry(
  steeringWheelGeometry.parameters.radius,
  steeringWheelGeometry.parameters.radius * 2.2,
  steeringWheelGeometry.parameters.radius / 2
);
const steeringWheelPinGeometry = new THREE.CylinderBufferGeometry(
  steeringWheelGeometry.parameters.tube / 2,
  steeringWheelGeometry.parameters.tube / 2,
  (steeringWheelGeometry.parameters.radius + steeringWheelGeometry.parameters.tube) * 2.75,
  20
);


const mainSailGeometry = new THREE.BufferGeometry();
const mainSailPositions = [
  [0, mastHeight / 3, 0],
  [0, mastHeight / 3, mastSupportLength],
  [0, mastHeight, 0],
].flat();

mainSailGeometry.setAttribute(
  "position",
  new THREE.BufferAttribute(new Float32Array(mainSailPositions), 3)
);
mainSailGeometry.computeVertexNormals();

const frontSailGeometry = new THREE.BufferGeometry();
const frontSailPositions = [
  [0, mastHeight / 3, -0.15],
  [0, boatHeight / 2, -(boatLength / 2 + bowLength)],
  [0, 0.8 * mastHeight, -0.5],
].flat();

const ropeGeometry = new THREE.BufferGeometry().setFromPoints([
  new THREE.Vector3(0, 0.8 * mastHeight, -0.5),
  new THREE.Vector3(0, 0.9 * mastHeight, 0),
]);

frontSailGeometry.setAttribute(
  "position",
  new THREE.BufferAttribute(new Float32Array(frontSailPositions), 3)
);
frontSailGeometry.computeVertexNormals();

const bowGeometry = new THREE.BufferGeometry();

const bowInternalGeometry = (() => {
  const corners = {
    left: -boatWidth / 2,
    right: boatWidth / 2,
    bottom: -boatHeight / 2,
    top: boatHeight / 2,
  };

  const vertices = [
    // square
    {pos: [corners.left, corners.bottom, 0], norm: [0, 0, 1]},
    {pos: [corners.right, corners.bottom, 0], norm: [0, 0, 1]},
    {pos: [corners.right, corners.top, 0], norm: [0, 0, 1]},

    {pos: [corners.right, corners.top, 0], norm: [0, 0, 1]},
    {pos: [corners.left, corners.top, 0], norm: [0, 0, 1]},
    {pos: [corners.left, corners.bottom, 0], norm: [0, 0, 1]},

    // right
    {pos: [corners.right, corners.top, 0], norm: [1, 0, 0]},
    {pos: [0, corners.top, bowLength], norm: [1, 0, 0]},
    {pos: [corners.right, corners.bottom, 0], norm: [1, 0, 0]},

    // left
    {pos: [corners.left, corners.top, 0], norm: [-1, 0, 0]},
    {pos: [0, corners.top, bowLength], norm: [-1, 0, 0]},
    {pos: [corners.left, corners.bottom, 0], norm: [-1, 0, 0]},

    // bottom
    {pos: [corners.left, corners.bottom, 0], norm: [0, -1, 0]},
    {pos: [0, corners.top, bowLength], norm: [0, -1, 0]},
    {pos: [corners.right, corners.bottom, 0], norm: [0, -1, 0]},

    // top
    {pos: [corners.left, corners.top, 0], norm: [0, 1, 0]},
    {pos: [0, corners.top, bowLength], norm: [0, 1, 0]},
    {pos: [corners.right, corners.top, 0], norm: [0, 1, 0]},
  ];

  const positions = [];
  const normals = [];
  vertices.forEach((vertex) => {
    positions.push(...vertex.pos);
    normals.push(...vertex.norm);
  });

  return {positions, normals};
})();

bowGeometry.setAttribute(
  "position",
  new THREE.BufferAttribute(new Float32Array(bowInternalGeometry.positions), 3)
);

bowGeometry.computeVertexNormals();

const sensorGeometry = new THREE.BufferGeometry();
sensorGeometry.setAttribute(
  "position",
  new THREE.BufferAttribute(
    new Float32Array([0, 0, 0]),
    3
  )
);

// MATERIAL

const bodyMaterial = new THREE.MeshPhongMaterial({
  color: 0xba8c63,
  side: THREE.DoubleSide,
});

const sensorMaterial = new THREE.PointsMaterial({color: 0x00ff00});

const sailMaterial = new THREE.MeshPhongMaterial({
  color: 0xffffff,
  side: THREE.DoubleSide,
});

const ropeMaterial = new THREE.MeshPhongMaterial({
  color: 0x000000,
});

const steeringWheelMaterial = new THREE.MeshPhongMaterial({color: 0xba8c63});

// MESH

const body = new THREE.Mesh(bodyGeometry, bodyMaterial);

const bowRear = new THREE.Mesh(bowGeometry, bodyMaterial);

const bowFront = new THREE.Mesh(bowGeometry, bodyMaterial);

const mast = new THREE.Mesh(mastGeometry, bodyMaterial);

const mastSupport = new THREE.Mesh(mastSupportGeometry, bodyMaterial);

const mainSail = new THREE.Mesh(mainSailGeometry, sailMaterial);

const frontSail = new THREE.Mesh(frontSailGeometry, sailMaterial);

const rope = new THREE.Line(ropeGeometry, ropeMaterial);

const steeringWheelConstruction = new THREE.Object3D();
const steeringWheel = new THREE.Mesh(steeringWheelGeometry, steeringWheelMaterial);
const steeringWheelBasis = new THREE.Mesh(steeringWheelBasisGeometry, bodyMaterial);
const steeringWheelPin1 = new THREE.Mesh(steeringWheelPinGeometry, steeringWheelMaterial);
const steeringWheelPin2 = new THREE.Mesh(steeringWheelPinGeometry, steeringWheelMaterial);
const steeringWheelPin3 = new THREE.Mesh(steeringWheelPinGeometry, steeringWheelMaterial);
const steeringWheelPin4 = new THREE.Mesh(steeringWheelPinGeometry, steeringWheelMaterial);

// Initial positioning

mast.position.set(0, mastHeight / 2, 0);

mastSupport.position.set(0, mastHeight / 3, mastSupportLength / 2);
mastSupport.rotateY(Math.PI / 2);
mastSupport.rotateZ(Math.PI / 2);

bowRear.position.set(0, 0, boatLength / 2);

bowFront.position.set(0, 0, -boatLength / 2);
bowFront.rotateY(Math.PI);

steeringWheelConstruction.position.z = boatLength / 4;
steeringWheelConstruction.position.y = boatHeight / 2;
steeringWheelConstruction.position.x = boatWidth / 4;
steeringWheel.position.y = steeringWheelGeometry.parameters.radius * 2;
steeringWheelPin2.rotateZ(Math.PI / 4);
steeringWheelPin3.rotateZ(Math.PI / 2);
steeringWheelPin4.rotateZ(-Math.PI / 4);
steeringWheelBasis.position.y = steeringWheelBasisGeometry.parameters.height / 2;
steeringWheelBasis.position.z = -steeringWheelGeometry.parameters.tube * 5;
boatSteeringCamera.position.z = boatLength / 4;
boatSteeringCamera.position.y = boatHeight / 2;

export const sensor = new THREE.Points(sensorGeometry, sensorMaterial);

export function changeBoatRotation(angle) {
  rotateBoat(-angle)
  rotateSteeringWheel(-angle)
}

function rotateBoat(angle) {
  const rotation = boat.rotation;
  rotation.y = +(THREE.MathUtils.lerp(rotation.y, rotation.y + angle, .2)).toFixed(2);
}

function rotateSteeringWheel(angle) {
  steeringWheel.rotation.z = THREE.MathUtils.lerp(steeringWheel.rotation.z, angle * 25, .02);
}

export function moveBoat(x) {
  boat.position.x += (x * environmentSpeed) / 2;
}

// ASSEMBLY LINE
boat.add(body);
boat.add(bowRear);
boat.add(bowFront);
boat.add(mast);
boat.add(mastSupport);
boat.add(mainSail);
boat.add(frontSail);
boat.add(rope);
boat.add(sensor);

steeringWheel.add(steeringWheelPin1);
steeringWheel.add(steeringWheelPin2);
steeringWheel.add(steeringWheelPin3);
steeringWheel.add(steeringWheelPin4);
steeringWheelConstruction.add(steeringWheel);
steeringWheelConstruction.add(steeringWheelBasis);
steeringWheelConstruction.add(boatSteeringCamera);
boat.add(steeringWheelConstruction);

boatCamera.position.set(0, boatHeight, -bowLength / 3);
boat.add(boatCamera);
