import * as THREE from "three";
import { gui } from "./gui";
import { OrbitControls } from "three/examples/jsm/controls/OrbitControls";
import { river } from "./river";
import { camera, cameras } from "./camera";
import { lightGroup, moveLight } from "./light";
import { boat, moveBoat, changeBoatRotation } from "./boat";
import { moveEnvironment } from "./environment";
import { groundObjects } from "./ground";
import { bird } from "./bird";
import { clearRaycasters, createRaycasters } from "./ray";
import { shipPositionToShipRotation } from "./fuzzy-controller";

const renderer = new THREE.WebGLRenderer({ alpha: true });
let fps = 0;

renderer.setSize(window.innerWidth, window.innerHeight);
document.body.appendChild(renderer.domElement);

const scene = new THREE.Scene();
scene.background = new THREE.Color(0x9fd2f1);
scene.fog = new THREE.FogExp2(0xb0e3ff, 0.03);

scene.add(bird);

new OrbitControls(camera, renderer.domElement);

// ENVIRONMENT
groundObjects.forEach((o) => scene.add(o));
scene.add(river);

// LIGHTS
scene.add(lightGroup);

// BOAT
boat.position.set(0, 0.5, 0);

scene.add(boat);

let raycasters;

// TEMP CONFIG
const tempConfig = {
  fuzzyLogic: true,
  logDistance: false,
  logPosition: false,
  logFPS: false,
  rayDistance: 10,
};
gui.add(tempConfig, "fuzzyLogic");
gui.add(tempConfig, "logDistance");
gui.add(tempConfig, "logPosition");
gui.add(tempConfig, "logFPS");
gui.add(tempConfig, "rayDistance").min(1).max(20);
// END TEMP CONFIG
const animate = function () {
  const boatSpeed = Math.sin(-boat.rotation.y);
  const envSpeed = 1 - Math.abs(boatSpeed);
  fps++;

  moveEnvironment(scene, envSpeed);
  moveBoat(boatSpeed);
  if (raycasters) clearRaycasters(scene);
  raycasters = createRaycasters(scene);
  moveLight(scene);
  bird.move();
  renderer.render(scene, cameras.cameras[cameras.selectedCameraId]);

  // TEMP
  if (tempConfig.fuzzyLogic) {
    const { leftCollision, rightCollision } = getCollisions();

    const distanceFromLeft = Math.min(
      leftCollision.distance,
      tempConfig.rayDistance
    );
    const distanceFromRight = Math.min(
      rightCollision.distance,
      tempConfig.rayDistance
    );

    if (tempConfig.logDistance)
      console.log(`L: ${distanceFromLeft} \t R: ${distanceFromRight}`);

    const distance = distanceFromLeft + distanceFromRight;

    // Ship position (values in range 0.0 - 10.0)
    const currentPosition = +(
      ((distanceFromLeft - distanceFromRight) / distance) * 5 +
      5
    ).toFixed(2);

    if (tempConfig.logPosition) console.log(`Position: ${currentPosition}`);

    const change = shipPositionToShipRotation(currentPosition);
    changeBoatRotation(change);
  }
  // END.TEMP

  moveBoat(boatSpeed);

  requestAnimationFrame(animate);
};

setInterval(() => {
  if (tempConfig.logFPS) console.log(`FPS: ${fps}`);
  fps = 0;
}, 1000);

function getCollisions() {
  const leftCollisions = raycasters.raycasterLeft.intersectObject(river);
  const rightCollisions = raycasters.raycasterRight.intersectObject(river);
  const leftCollision = leftCollisions[leftCollisions.length - 1];
  const rightCollision = rightCollisions[rightCollisions.length - 1];

  return { leftCollision, rightCollision };
}

animate();
