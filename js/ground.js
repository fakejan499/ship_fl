import * as CAMERA from "./camera";
import * as THREE from "three";
import {environmentSpeed} from "./environment";
import {Cactus} from "./cactus";
import {Palm} from "./palm";
import {gui} from "./gui";

const config = {
  lengthPerPlant: 20,
  widthPerPlant: 35
}
const envGui = gui.addFolder('environment');
envGui.add(config, 'lengthPerPlant').min(15).max(120);
envGui.add(config, 'widthPerPlant').min(15).max(120);


const groundLength = CAMERA.far + CAMERA.z;
const groundWidth = 2000;
const groundStartPositionZ = -CAMERA.z / (5 / 4);

const groundShape = new THREE.PlaneGeometry(groundWidth, groundLength);
const groundMaterial = new THREE.MeshPhongMaterial({ color: 0xffe066 });

export const groundObjects = initGroundObjects();

function initGroundObjects() {
  const objs = [];
  const numberOfGroundObjects = 3;
  for (let i = 0; i < numberOfGroundObjects; i++) {
    objs.push(createNextMesh(objs))
  }
  return objs;
}

function createNextMesh(arr = groundObjects) {
  const groundMesh = new THREE.Mesh(groundShape, groundMaterial);
  groundMesh.rotateX(-Math.PI / 2);
  if (arr.length > 0) groundMesh.position.z = arr[arr.length - 1].position.z - groundLength
  else groundMesh.position.z = groundStartPositionZ;
  addVegetationToGround(groundMesh)
  return groundMesh;
}

function addVegetationToGround(ground) {
  const plants = [Cactus, Palm, Palm];
  const {lengthPerPlant, widthPerPlant} = config;

  for (let i = -groundLength / 2; i < groundLength / 2; i += lengthPerPlant) {
    // because the ground so wide, it does not make sense to generate cacti on the whole ground
    for (let j = - groundWidth / 12; j < groundWidth / 12; j += widthPerPlant) {
      const y = Math.floor(Math.random() * lengthPerPlant) + i;
      const x = Math.floor(Math.random() * widthPerPlant) + j;
      const plant = new plants[Math.floor(Math.random() * 2)];
      plant.position.set(x, y, 0);
      ground.add(plant);
    }
  }
}

export function moveGround(scene, speedMul) {
  groundObjects.forEach(o => {
    o.position.z += environmentSpeed * speedMul;
  });

  if (firstGroundObjMovedOutOfView()) {
    removeFirstAndAddNewGroundObj(scene);
  }
}

function firstGroundObjMovedOutOfView() {
  return groundObjects[0].position.z - groundLength > groundStartPositionZ / 2;
}

function removeFirstAndAddNewGroundObj(scene) {
  scene.remove(groundObjects[0]);
  groundObjects[0].clear();
  groundObjects.splice(0, 1);
  const nextGroundMesh = createNextMesh();
  groundObjects.push(nextGroundMesh);
  scene.add(nextGroundMesh);
}
