import * as THREE from "three";
import * as CAMERA from './camera';
import {birdCamera} from "./camera";

const wingSize = 1;
const trunkLength = 1.75;
const trunkWidth = .125;
const trunkHeight = .125;

const material = new THREE.MeshBasicMaterial({color: 0x444444, side: THREE.DoubleSide});
const wingShape = new THREE.Shape();
wingShape.moveTo(wingSize / 2, 0);
wingShape.lineTo(0, wingSize);
wingShape.lineTo(-wingSize / 2, 0);

const trunkGeometry = new THREE.BoxGeometry(trunkWidth, trunkHeight, trunkLength);
trunkGeometry.translate(0, 0, (wingSize - trunkLength) / 4)
const wingGeometry = new THREE.ExtrudeGeometry(wingShape, {depth: 1});
wingGeometry.scale(1,1, .1)
wingGeometry.rotateX(Math.PI / 2);
wingGeometry.rotateY(-Math.PI / 2);


class Bird extends THREE.Object3D {
  constructor() {
    super();
    this.trunk = new THREE.Mesh(trunkGeometry, material);
    this.leftWing = new THREE.Mesh(wingGeometry, material);
    this.rightWing = new THREE.Mesh(wingGeometry, material);
    this.rightWing.scale.set(-1, 1, 1);

    this.wingDirection = Math.random() > 0 ? 1 : -1;
    this.wingStep = Math.PI / Math.floor(Math.random() * 50 + 175);
    this.initWingsRotation();

    this.add(this.leftWing)
    this.add(this.rightWing)
    this.add(this.trunk)
  }

  initWingsRotation() {
    const state = Math.random() * Math.PI / 10;

    this.rightWing.rotateZ(state);
    this.leftWing.rotateZ(-state);
  }

  rotateWings() {
    this.rightWing.rotateZ(this.wingStep * this.wingDirection);
    this.leftWing.rotateZ(-this.wingStep * this.wingDirection);

    if (this.rightWing.rotation.z > Math.PI / 10 || this.rightWing.rotation.z < -Math.PI / 10) this.wingDirection = -this.wingDirection;
  }
}

class Flock extends THREE.Group {
  constructor() {
    super();
    this.initPosition();
    this.initBirds();
    this.waiting = false;
  }

  initPosition() {
    this.position.y = Math.floor(Math.random() * 6) + 20; // < 20 ; 25 >
    this.position.x = Math.round(Math.random() * 20) - 10; // < -10 ; 10 >
    this.position.z = Math.round(Math.random() * 5); // < 0 ; 5 >

    do {
      this.direction = {
        x: Math.floor(Math.random() * 3) - 1, // x = (-1;0;1)
        z: Math.floor(Math.random() * 2) - 1, // x = (-1;0)
        y: 0
      }
    } while (this.direction.x === 0 && this.direction.z === 0);
    this.rotation.y = Math.PI / 2 * -this.direction.x;
    if (this.direction.z !== 0) this.rotation.y /= 2;
  }

  initBirds() {
    this.birds = [new Bird()];

    for (let i = 0; i <= 4; i++) {
      const bird = new Bird();
      const side = i % 2 === 0 ? -1 : 1;
      bird.position.x = (Math.floor(i / 2) + 1) * side * wingSize * 2.5;
      bird.position.z = (Math.floor(i / 2) + 1) * trunkLength * 1.5;
      bird.position.y = Math.random() * 2 - 1;
      this.birds.push(bird);
    }

    this.addBirdCamera();

    this.add(...this.birds);
  }

  addBirdCamera() {
    this.birds[this.birds.length - 1].trunk.add(birdCamera);
    birdCamera.position.z = -trunkLength / 2;
    birdCamera.position.y = trunkHeight * 2;
  }

  move() {
    if (this.waiting) return;

    const div = 10
    this.position.x += this.direction.x / div;
    this.position.y += this.direction.y / div;
    this.position.z += this.direction.z / div;

    this.birds.forEach(b => b.rotateWings());

    if (bird.beyondView()) {
      this.waiting = true;
      setTimeout(() => {
        this.waiting = false;
        this.initPosition();
      }, Math.floor(Math.random() * 20 + 1) * 1000); // 1s - 20s
    }
  }

  beyondView() {
    return Math.sqrt(this.position.x ** 2 + this.position.z ** 2) > Math.abs(CAMERA.far) + Math.abs(CAMERA.z);
  }

}

export const bird = new Flock();
