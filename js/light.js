import * as THREE from "three";
import {gui} from "./gui";

export const lightGroup = new THREE.Object3D();

const sunLightWrapper = new THREE.Object3D();
const sunLight = new THREE.PointLight(new THREE.Color(.6, .6, .6), 1, 0, 2);
sunLight.position.set(0, 150, 0);
sunLightWrapper.rotateX(Math.PI / 9);
sunLightWrapper.add(sunLight);
lightGroup.add(sunLightWrapper);

const ambient = new THREE.HemisphereLight(0xffffbb, 0x080820, .8);
lightGroup.add(ambient)

const config = {
  step: Math.PI / 1024,
  enabled: false,
};
const guiFolder = gui.addFolder('light');
guiFolder.add(config, 'step').min(0).max(Math.PI / 2 ** 7).step(Math.PI / 1024);
guiFolder.add(config, 'enabled');

export function moveLight(scene) {
  if (config.enabled) {
    sunLightWrapper.rotateZ(config.step);
    const angle = sunLightWrapper.rotation.z % (2 * Math.PI);

    if(angle > 0) {
      const intensity = 1 - config.step / 3;
      scene.background.multiply(new THREE.Color(intensity, intensity, intensity));
      scene.fog.color.multiply(new THREE.Color(intensity, intensity, intensity));
    } else {
      const intensity = 1 + config.step / 3;
      scene.background.multiply(new THREE.Color(intensity, intensity, intensity));
      scene.fog.color.multiply(new THREE.Color(intensity, intensity, intensity));
    }

    ambient.intensity = Math.max(Math.cos(angle), .3);
    sunLight.intensity = Math.min((Math.cos(angle) + 1) / 2, .7);
  }
}
