import * as THREE from "three";
import { boat, boatLength, bowLength, sensor } from "./boat";
import {gui} from "./gui";

const sensorX = 1;
const sensorZ = -1;

let raycasterLeft = new THREE.Raycaster();
let raycasterRight = new THREE.Raycaster();

let leftHelper;
let rightHelper;

const config = {
  showHelpers: false
}
gui.addFolder('ray').add(config, 'showHelpers')

export function createRaycasters(scene) {
  // Sensor -- raycasters
  let sensorPosition = new THREE.Vector3(
    ...sensor.geometry.attributes.position.array
  );
  sensorPosition.applyAxisAngle(new THREE.Vector3(0, 1, 0), boat.rotation.y);
  sensorPosition.x = boat.position.x - Math.sin(boat.rotation.y) * boatLength;

  const raycasterRightVector = new THREE.Vector3(sensorX, 0, sensorZ);
  raycasterRightVector.applyAxisAngle(
    new THREE.Vector3(0, 1, 0),
    boat.rotation.y
  );

  const raycasterLeftVector = new THREE.Vector3(-sensorX, 0, sensorZ);
  raycasterLeftVector.applyAxisAngle(
    new THREE.Vector3(0, 1, 0),
    boat.rotation.y
  );

  raycasterLeft = new THREE.Raycaster(sensorPosition, raycasterLeftVector);
  raycasterRight = new THREE.Raycaster(sensorPosition, raycasterRightVector);

  if (config.showHelpers) {
    leftHelper = new THREE.ArrowHelper(
      raycasterLeft.ray.direction,
      raycasterLeft.ray.origin,
      20,
      0xff0000
    );
    rightHelper = new THREE.ArrowHelper(
      raycasterRight.ray.direction,
      raycasterRight.ray.origin,
      20,
      0xff0000
    );
    scene.add(rightHelper);
    scene.add(leftHelper);
  }

  return { raycasterLeft, raycasterRight };
}

export function clearRaycasters() {
  leftHelper?.clear();
  rightHelper?.clear();
}
