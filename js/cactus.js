import * as THREE from 'three';

const segmentGeometry = new THREE.BoxGeometry();
const CACTUS_MATERIALS = [
  new THREE.MeshPhongMaterial({color: '#237543'}),
  new THREE.MeshPhongMaterial({color: 'rgb(91,111,85)'}),
  new THREE.MeshPhongMaterial({color: '#09944e'}),
  new THREE.MeshPhongMaterial({color: '#a2d859'}),
]
const spikeGeometry = new THREE.BoxGeometry(.1, 1.2, .1);
const diagonalSpikeGeometry = new THREE.BoxGeometry(.1, Math.sqrt(2) + .2, .1);
const spikeMaterial = new THREE.MeshBasicMaterial({color: '#333'});
const spikePrototypeX = new THREE.Mesh(spikeGeometry, spikeMaterial);
const diagonalSpikePrototypeX = new THREE.Mesh(diagonalSpikeGeometry, spikeMaterial);
const spikePrototypeY = new THREE.Mesh(spikeGeometry, spikeMaterial);
const diagonalSpikePrototypeY = new THREE.Mesh(diagonalSpikeGeometry, spikeMaterial);

spikePrototypeX.rotateX(Math.PI / 2);
diagonalSpikePrototypeX.rotateY(Math.PI / 4);
diagonalSpikePrototypeX.rotateX(Math.PI / 2);
spikePrototypeY.rotateX(Math.PI / 2);
spikePrototypeY.rotateZ(Math.PI / 2);
diagonalSpikePrototypeY.rotateY(-Math.PI / 4);
diagonalSpikePrototypeY.rotateX(Math.PI / 2);

export const cacti = new Set();

export class Cactus extends THREE.Object3D {
  constructor() {
    super();

    const segments = this.generateCactusSegments();

    segments.forEach((seg, i) => {
      seg.position.y = i;

      this.add(seg)
    });

    this.rotateX(Math.PI / 2)
    this.rotateY(Math.floor(Math.random() * Math.PI));

    cacti.add(this);

    this.addEventListener('removed', () => {
      cacti.delete(this);
    })
  }

  generateCactusSegments() {
    const maxSegments = 6;
    const numberOfSegments = Math.floor(Math.random() * maxSegments) + 1;
    const segments = [];
    const material = CACTUS_MATERIALS[Math.floor(Math.random() * CACTUS_MATERIALS.length)];

    for (let i = 0; i < numberOfSegments; i++) {
      const segment = new THREE.Mesh(
        segmentGeometry,
        material
      );

      this.addSpikesToSegment(segment, spikePrototypeX);
      this.addSpikesToSegment(segment, spikePrototypeY);
      this.addSpikesToSegment(segment, diagonalSpikePrototypeX);
      this.addSpikesToSegment(segment, diagonalSpikePrototypeY);
      segments.push(segment);
    }

    return segments;
  }

  addSpikesToSegment(seg, prototype) {
    const bottomSpike = prototype.clone();
    const centerSpike = prototype.clone();
    const topSpike = prototype.clone();

    bottomSpike.translateZ(-.32);
    topSpike.translateZ(.32);

    seg.add(bottomSpike);
    seg.add(centerSpike);
    seg.add(topSpike);
  }

  clear() {
    cacti.delete(this);

    return super.clear();
  }
}
