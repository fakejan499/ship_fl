import * as THREE from "three";
import {gui} from "./gui";

export const far = 75;
export const z = 20;

export const camera = new THREE.PerspectiveCamera(80, window.innerWidth / window.innerHeight, 0.1, far);
camera.position.set(0, 10, z);

export const boatCamera = new THREE.PerspectiveCamera(100, window.innerWidth / window.innerHeight, 0.1, far);

export const boatSteeringCamera = new THREE.PerspectiveCamera(75, window.innerWidth / window.innerHeight, 0.1, far);

export const birdCamera =  new THREE.PerspectiveCamera(100, window.innerWidth / window.innerHeight, 0.1, far);

export const cameras = {
  selectedCameraId: 0,
  cameras: [camera, boatSteeringCamera, boatCamera, birdCamera]
}
gui.addFolder('camera').add(cameras, 'selectedCameraId').min(0).max(cameras.cameras.length - 1).step(1);
