import {moveRiver} from "./river";
import {moveGround} from "./ground";

export const environmentSpeed = .5;

export function moveEnvironment(scene, speedMul = 1) {
  moveGround(scene, speedMul);
  moveRiver(speedMul);
}
